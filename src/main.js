import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'reset-css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import apis from '@/http/apis'
import AdminBreadcrumb from '@/components/AdminBreadcrumb'
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import Highlight from './h.js';
Vue.use(Highlight);
import JsonExcel from "vue-json-excel";
Vue.component("downloadExcel", JsonExcel);
import 'froala-editor/js/plugins.pkgd.min.js';
//Import third party plugins
import 'froala-editor/js/third_party/embedly.min';
import 'froala-editor/js/third_party/font_awesome.min';
import 'froala-editor/js/third_party/spell_checker.min';
import 'froala-editor/js/third_party/image_tui.min';
// Import Froala Editor css files.
import 'froala-editor/css/froala_editor.pkgd.min.css';

// Import and use Vue Froala lib.
import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala)
import VueCropper from 'vue-cropper'
Vue.use(VueCropper)
//vue-echarts
import ECharts from 'vue-echarts'
import {
  use
} from 'echarts/core'
// 手动引入 ECharts 各模块来减小打包体积

import {
  CanvasRenderer
} from 'echarts/renderers'
import {
  BarChart,
  LineChart
} from 'echarts/charts'
import {
  GridComponent,
  TooltipComponent,
  TitleComponent,
  LegendComponent,
  ToolboxComponent
} from 'echarts/components'

use([
  CanvasRenderer,
  BarChart,
  GridComponent,
  TooltipComponent,
  TitleComponent,
  LegendComponent,
  ToolboxComponent,
  LineChart
]);
Vue.component('v-chart', ECharts)
//hightcharts
import HighchartsVue from 'highcharts-vue'
Vue.use(HighchartsVue)
Vue.component('AdminBreadcrumb', AdminBreadcrumb)
Vue.config.productionTip = false
Vue.prototype.$ELEMENT = {
  size: 'small',
  zIndex: 3000
};
Vue.use(ElementUI)
Vue.use(ViewUI);
Vue.prototype.$apis = apis
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')