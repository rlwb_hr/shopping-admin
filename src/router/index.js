import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'
Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/users',
    children: [{
      path: 'users',
      name: 'Users',
      component: () => import( /* webpackChunkName: "users" */ '../views/Users.vue')
    }, {
      path: 'goods',
      name: 'Goods',
      component: () => import( /* webpackChunkName: "goods" */ '../views/Goods.vue')
    }, {
      path: 'addgoods',
      name: 'Addgoods',
      component: () => import( /* webpackChunkName: "goods" */ '../views/AddGoods.vue')
    }, {
      path: 'roles',
      name: 'Roles',
      component: () => import( /* webpackChunkName: "roles" */ '../views/Roles.vue')
    }, {
      path: 'rights',
      name: 'Rights',
      component: () => import( /* webpackChunkName: "rights" */ '../views/Rights.vue')
    }, {
      path: 'reports',
      name: 'Reports',
      component: () => import( /* webpackChunkName: "reports" */ '../views/Reports.vue')
    }]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import( /* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/upload',
    name: 'Upload',
    component: () => import( /* webpackChunkName: "login" */ '../views/Upload.vue')
  }
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

function patchRouterMethod(router, methodName) {
  router['old' + methodName] = router[methodName]
  router[methodName] = async function (location) {
    return router['old' + methodName](location).catch((error) => {
      if (error.name === 'NavigationDuplicated') {
        return this.currentRoute
      }
      throw error
    })
  }
}

patchRouterMethod(router, 'push')
patchRouterMethod(router, 'replace')

router.beforeEach((to, from, next) => {
  if (to.name !== 'Login') {
    if (!store.state.token) {
      next({
        replace: true,
        name: 'Login'
      })
      return;
    }
    next()
    return;
  }
  next()
})

export default router