import http from './index'
import store from '../store'
import {
    SET_INFO
} from '../store/mutation-type'
import router from '../router'
import {
    Message
} from 'element-ui'
export default {
    //登录
    login: (data, message) => {
        http('login', data, 'POST')
            .then(({
                meta: {
                    msg
                },
                data: {
                    token,
                    username
                }
            }) => {
                message({
                    type: 'success',
                    message: msg
                })
                store.commit(SET_INFO, {
                    token,
                    username
                })
                router.push({
                    name: 'Home'
                })
            }).catch(err => {
                message({
                    type: 'error',
                    message: err
                })
            })
    },
    // 获取左侧权限菜单
    async getMenus() {
        const res = await http('menus', null, 'GET')
        return res.data;
    },
    //获取用户列表
    async getUsers(params) {
        const res = await http('users', null, 'GET', params)
        return res.data
    },
    //修改用户状态
    async updateState({
        id,
        mg_state
    }) {
        const res = await http(`users/${id}/state/${mg_state}`, null, 'PUT')
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    },
    //查询用户信息
    async getUserInfo(id) {
        const res = await http(`users/${id}`, null, 'GET')
        return res.data
    },
    //获取角色列表
    async getRoles() {
        const res = await http(`roles`, null, 'GET')
        return res.data
    },
    //编辑角色
    async editRole(obj) {
        const res = await http(`users/${obj.id}/role`, {
            rid: obj.rid
        }, 'PUT')
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    },
    //获取角色列表
    async getRoles() {
        const res = await http(`roles`, null, 'GET')
        return res.data
    },
    //删除权限菜单
    async delRights({
        roleId,
        rightId
    }) {
        const res = await http(`roles/${roleId}/rights/${rightId}`, null, 'DELETE')
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    },
    //获取所有权限列表
    async getAllRights() {
        const res = await http(`rights/tree`, null, 'GET')
        return res.data
    },
    //设置角色权限
    async setRights({
        roleId,
        rids
    }) {
        const res = await http(`roles/${roleId}/rights`, {
            rids
        }, 'POST')
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    },
    //获取商品所有分类
    async getAllCats() {
        const res = await http(`categories`, null, 'GET', {
            type: 3
        })
        return res.data
    },
    //获取商品的attrs

    async getGoodsAttrs(id, sel) {
        const res = await http(`categories/${id}/attributes`, null, 'GET', {
            sel
        })
        return res.data
    },
    //添加商品
    async addGoods(obj) {
        const res = await http(`goods`, obj, 'POST')
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    },
    //上传方法
    async upload(obj) {
        const res = await http(`upload`, obj, 'POST', null, 'application/x-www-form-urlencoded')
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    },

    //数据统计
    async lineChart() {
        const res = await http(`reports/type/1`, null, 'GET')
        return res.data
    },
}