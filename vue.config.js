const path = require('path')
const resolve = dir => path.join(__dirname, dir)
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const isProduction = process.env.NODE_ENV === 'production'
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const productionGzipExtensions = ['js', 'css'];
// externals
const externals = {
    "vue": 'Vue',
    "view-design": 'iview',
    "iview": 'ViewUI',
}
// CDN外链，会插入到index.html中
const cdn = {
    // 开发环境
    dev: {
        css: [],
        js: []
    },
    // 生产环境
    build: {
        css: [
            // 'https://cdn.jsdelivr.net/npm/vant@2.12/lib/index.css',
            'https://cdn.bootcdn.net/ajax/libs/view-design/4.5.0/styles/iview.min.css'
        ],
        js: [
            'https://cdn.jsdelivr.net/npm/vue@2.6.11/dist/vue.min.js',
            // 'https://cdn.jsdelivr.net/npm/vue-router@3.1.5/dist/vue-router.min.js',
            // 'https://cdn.jsdelivr.net/npm/axios@0.19.2/dist/axios.min.js',
            // 'https://cdn.jsdelivr.net/npm/vuex@3.1.2/dist/vuex.min.js',
            // 'https://cdn.jsdelivr.net/npm/vant@2.12/lib/vant.min.js',
            'https://cdn.bootcdn.net/ajax/libs/view-design/4.5.0/iview.min.js'
        ]
    }
}


module.exports = {
    configureWebpack: config => {
        // 为生产环境修改配置...
        const plugins = [];
        if (isProduction) {
            // externals
            config.externals = externals
            //plugin
            plugins.push(
                new CompressionWebpackPlugin({
                    filename: "[path].gz[query]",
                    algorithm: "gzip",
                    test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
                    threshold: 10240,
                    minRatio: 0.8
                })
            );
            // 开启分离js
            config.optimization = {
                runtimeChunk: 'single',
                splitChunks: {
                    chunks: 'all',
                    maxInitialRequests: Infinity,
                    minSize: 1000 * 60,
                    cacheGroups: {
                        vendor: {
                            test: /[\\/]node_modules[\\/]/,
                            name(module) {
                                // 排除node_modules 然后吧 @ 替换为空 ,考虑到服务器的兼容
                                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
                                return `npm.${packageName.replace('@', '')}`
                            }
                        }
                    }
                }
            }

        }
        config.plugins = [...config.plugins, ...plugins];
    },
    chainWebpack: config => {
        /**
         * 添加CDN参数到htmlWebpackPlugin配置中
         */
        config.plugin('html').tap(args => {
            if (isProduction) {
                args[0].cdn = cdn.build
            } else {
                args[0].cdn = cdn.dev
            }
            return args
        })
        config.optimization.delete('splitChunks')
        // 添加别名
        config.resolve.alias
            .set('_', resolve('src'))

        if (isProduction) {
            config.plugin('webpack-report').use(BundleAnalyzerPlugin, [{
                analyzerMode: 'static'
            }])
        }
    },
    //去除生产环境的productionSourceMap
    productionSourceMap: false,
    devServer: {
        proxy: {
            // '/api': {
            //     target: 'http://localhost:8889/', //对应自己的接口
            //     changeOrigin: true,
            //     ws: true,
            //     pathRewrite: {
            //         '^/api': '/api'
            //     }
            // },
            '/cnode': {
                target: 'https://cnodejs.org', //对应自己的接口
                changeOrigin: true,
                ws: true,
                secure: true,
                pathRewrite: {
                    '^/cnode': ''
                }
            },
            '/admin': {
                target: 'http://localhost:8889', //对应自己的接口
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^/admin': ''
                }
            }
        }
    },
    publicPath: './'
}